import paho.mqtt.client as mqtt
from random import uniform
from peewee import *
import json, csv, requests, schedule

mqttc = mqtt.Client("python_pub")
mqttc.connect("test.mosquitto.org",1883)
mqttc.loop(2)

#publish random payload data between o to 500
data = int(uniform(0,500))
payload = data

db = SqliteDatabase('messages.db')

class MessageMQTT(Model):
    topic = CharField()
    message = CharField()
    time = CharField()

    class Meta:
        database = db

def on_connect(client, userdata, rc):
    print("connected with result code" + str(rc))

    

def on_message(client, userdata, msg):
    print "Topic:",msg.topic + '\nMessage:' + '\n{payload:' +  str(msg.payload) + ',' +  'timestamp:' + str(long(msg.timestamp)) + '}'
    message = MessageMQTT(topic = msg.topic, message = msg.payload, time = msg.timestamp)
    message.create_table()
    message.save()
    
    with open('messages.csv','a')as csvfile:
        spamwriter = csv.writer(csvfile,dialect = 'excel')
        spamwriter.writerow([msg.topic] + [str(msg.payload)] + [str(long(msg.timestamp))])
    
    key = "key-4a480b5dcb7a1c44e4b4d1341be27d3a"
    sandbox = "sandboxf35ebda54a494a44a83860938ce69655.mailgun.org"
    recipient = "nasarudinabdulshukor@gmail.com"

    request_url = 'https://api.mailgun.net/v2/{0}/messages'.format(sandbox)
    request = requests.post(request_url, auth=('api', key),files = [("attachment", open("messages.csv"))],
         data={
        'from': 'hello@example.com',
        'to': recipient,
        'subject': 'Hello',
        'text': 'csv test email'
    })
    print 'Status: {0}'.format(request.status_code)
    print 'Body:   {0}'.format(request.text)
    
def on_publish(client, userdata, msg):
    print("published " + str(msg))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("test.mosquitto.org",1883,60)
client.subscribe("MyChannel")
def message():
    client.publish("MyChannel", json.dumps(payload))
    return schedule.CancelJob
    
#publish message every day at 11:59pm
schedule.every().day.at('23:59').do(message)

while True:
    schedule.run_pending()
    time.sleep(1)

client.loop_forever()